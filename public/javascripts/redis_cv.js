let xhrCamera = new XMLHttpRequest();
let xhrMisc = new XMLHttpRequest();

let blobCamera, frCamera = new FileReader();
let imgFace = document.getElementById("imageFace");

let pic = imgFace.src;

let keyMain = 'main_cam';

let mainCamOn = false;

document.addEventListener('DOMContentLoaded', function () {
  readCameraAll();
}, false);


async function readCameraAll() {
  let x;
  while (true) {
    x = await readCamera(keyMain);
    imgFace.setAttribute("src", pic);
  }
}

function readCamera(imageID) {
  return new Promise(function (resolve, reject) {
    resolve(getImage(imageID).then(function (response) {
      pic = response;
    }, function (Error) {
      console.log(Error);
    }));
  });
}

function controlMainCam(camera) {
  if (camera === 'off') {
    mainCamOn = false;
    send_camera_message("main_cam", "stop");
    console.log("Main Camera Off");
  } else {
    mainCamOn = true;
    keyMain = camera;
    send_camera_message("main_cam", "start");
    console.log('Camera being used: ', keyMain);
  }
}


function getImage(imageChannel) {
  let url = "http://localhost:3000/video/" + imageChannel;
  console.log(url);
  console.log('Getting image blob from - ', url);
  xhrCamera.open('GET', url, true);
  xhrCamera.responseType = "blob";
  return new Promise(function (resolve, reject) {
    xhrCamera.onload = function () {
      if (xhrCamera.status === 200) {
        blobCamera = this.response;
        frCamera.onload = function (evt) {
          let result = evt.target.result;
          resolve(result);
        };
        frCamera.readAsDataURL(blobCamera);
      }
      else {
        reject(xhrCamera.statusText);
      }
    };
    xhrCamera.send(null);
  });
}

function send_camera_message(device, cmd) {
  console.log('Sending Camera Message => device: ' + device + ', command: ' + cmd);
  sendMessage("hw_in", '{"device":"' + device + '","action":"' + cmd + '", "params":null}');
}

function sendMessage(msgChannel, msgString) {
  xhrMisc.open('GET', "http://127.0.0.1:7379/PUBLISH/" + msgChannel + "/" + msgString, true);
  xhrMisc.send(null)
}
