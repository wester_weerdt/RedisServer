let express = require('express');
let router = express.Router();
let redis = require('redis');
let client = redis.createClient(6379, '127.0.0.1', {'return_buffers': true});


client.on('connect', function() {
  console.log('Connected to the Redis Server');
});


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/video/:loc', function (req, res, next) {
  let loc = req.params.loc;
  client.get(loc, function(err, reply) {
    if (err) {
      console.log("ERROR: " + loc + " does not exist - " + err.message);
    }
    else {
      res.contentType('image/jpeg');
      res.send(reply);
    }
  });

});


module.exports = router;
