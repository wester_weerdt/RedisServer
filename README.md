# Simple Node.js App to evaluate the video streaming from redis

## HowTo

1. Clone this repo and run `npm install` to install all packages.

2. Start the **HWM** and enable the main camera service on the **WebKioskSimulator**

3. Run `node bin/www` in this directory to get this server running.

3. Visit `localhost:3000` to see live video on the browser.

## How it works

The html page just sends HTTP GET request to `/video/main_cam` endpoint, and the backend node app returns the redis buffer in the format of `image/jpeg`.

